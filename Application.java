//
// COPYRIGHT (C) 2017-2018 NEC CORPORATION
//
// ALL RIGHTS RESERVED BY NEC NEC CORPORATION, THIS PROGRAM
// MUST BE USED SOLELY FOR THE PURPOSE FOR WHICH IT WAS
// FURNISHED BY NEC NEC CORPORATION, NO PART OF THIS PROGRAM
// MAY BE REPRODUCED OR DISCLOSED TO OTHERS, IN ANY FORM
// WITHOUT THE PRIOR WRITTEN PERMISSION OF NEC NEC CORPORATION.
//
// NEC CORPORATION CONFIDENTIAL AND PROPRIETARY
package com.nec.jp.systemdirector.ent.msa.account;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nec.jp.systemdirector.ent.msa.account.common.JenkinsBuildIdentifier;

/**
 * Spring Bootアプリケーションのエントリポイントとなるクラス。
 * 
 */
@SpringBootApplication

// SDE-MSA-PRIN サービス呼び出し回数を制限する (MSA-PRIN-ID-04)
//@EnableCaching
@MapperScan({ "com.nec.jp.systemdirector.ent.msa.account.repositories.mapper" })
public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    /**
     * マニフェストファイルからビルド情報を読み込み、システムプロパティに設定します。
     * 
     * @param mf マニフェストのオブジェクト。
     */
    public static void setBuildTags(Manifest mf) {
	if (mf != null) {
	    Attributes attr = mf.getMainAttributes();

	    System.setProperty(JenkinsBuildIdentifier.JENKINS_JOB_NAME,
		    attr.getValue(JenkinsBuildIdentifier.JENKINS_JOB_NAME) != null
			    ? attr.getValue(JenkinsBuildIdentifier.JENKINS_JOB_NAME)
			    : "");
	    System.setProperty(JenkinsBuildIdentifier.JENKINS_BUILD_NUMBER,
		    attr.getValue(JenkinsBuildIdentifier.JENKINS_BUILD_NUMBER) != null
			    ? attr.getValue(JenkinsBuildIdentifier.JENKINS_BUILD_NUMBER)
			    : "");
	    System.setProperty(JenkinsBuildIdentifier.JENKINS_BUILD_TAG,
		    attr.getValue(JenkinsBuildIdentifier.JENKINS_BUILD_TAG) != null
			    ? attr.getValue(JenkinsBuildIdentifier.JENKINS_BUILD_TAG)
			    : "");
	}
    }

    /**
     * mainメソッド。 Spring Bootアプリケーションの起動処理を実行します。
     * 
     * @param args コマンドライン引数。
     */
    @SuppressWarnings("squid:S2095")
    public static void main(String[] args) {
	try {
	    // ビルド情報を取得する
	    InputStream is = Application.class.getResourceAsStream("/META-INF/MANIFEST.MF");
	    Manifest mf = new Manifest(is);

	    setBuildTags(mf);
	} catch (IOException e) {
	    logger.warn("Failed to get MANIFEST.MF", e);
	}

	SpringApplication.run(Application.class, args);
    }

}
