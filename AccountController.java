//
// COPYRIGHT (C) NEC CORPORATION
//
// ALL RIGHTS RESERVED BY NEC NEC CORPORATION, THIS PROGRAM
// MUST BE USED SOLELY FOR THE PURPOSE FOR WHICH IT WAS
// FURNISHED BY NEC NEC CORPORATION, NO PART OF THIS PROGRAM
// MAY BE REPRODUCED OR DISCLOSED TO OTHERS, IN ANY FORM
// WITHOUT THE PRIOR WRITTEN PERMISSION OF NEC NEC CORPORATION.
//
// NEC CORPORATION CONFIDENTIAL AND PROPRIETARY
package com.nec.jp.systemdirector.ent.msa.account.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.cors.CorsConfiguration;

import com.nec.jp.systemdirector.ent.msa.account.resources.dto.AccountPostDto;
import com.nec.jp.systemdirector.ent.msa.account.resources.dto.AccountPostResponseDto;
import com.nec.jp.systemdirector.ent.msa.account.resources.http.SecureHttpHeaders;
import com.nec.jp.systemdirector.ent.msa.account.services.AccountService;
import com.nec.jp.systemdirector.ent.msa.account.services.dto.AccountDto;

/**
 * API v1 処理を実装するクラス。<br />
 * URI が <code>/api/v1/localized/customers/{id}/accounts</code> の API 処理を実装する。
 *
 */
@RestController
@Validated
@RefreshScope
@RequestMapping("${api.base-path}/customers/{cif}/accounts")
@CrossOrigin(origins = { "http://localhost:9000" }, methods = { RequestMethod.POST, RequestMethod.GET,
	RequestMethod.PUT,
	RequestMethod.DELETE }, allowCredentials = "true", allowedHeaders = CorsConfiguration.ALL, exposedHeaders = {}, maxAge = 1800)
public class AccountController {

    /** ロガー */
    @SuppressWarnings("squid:S1068")
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "X-Requested-With=XMLHttpRequest")
    @NewSpan("AccountController.post")
    public ResponseEntity<AccountPostResponseDto> post(@PathVariable("cif") @SpanTag("cif") String cif,
	    @RequestBody @Validated @SpanTag("input") AccountPostDto dto) {
	SecureHttpHeaders headers = new SecureHttpHeaders();
	headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
	AccountDto serviceDto = new AccountDto();
	BeanUtils.copyProperties(dto, serviceDto);
	// ID分解(1～4:店番号 5～:顧客番号)
	serviceDto.setBranchNumber(cif.substring(0, 4));
	serviceDto.setCustomerNumber(cif.substring(4));

	// AccountServiceの呼び出し
	AccountDto retServiceDto = accountService.register(serviceDto);

	if (retServiceDto == null) {
	    return new ResponseEntity<>(null, headers, HttpStatus.CONFLICT);
	}

	AccountPostResponseDto retDto = new AccountPostResponseDto();
	BeanUtils.copyProperties(retServiceDto, retDto);

	return new ResponseEntity<>(retDto, headers, HttpStatus.CREATED);
    }

}
