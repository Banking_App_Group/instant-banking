//
// COPYRIGHT (C) NEC CORPORATION
//
// ALL RIGHTS RESERVED BY NEC NEC CORPORATION, THIS PROGRAM
// MUST BE USED SOLELY FOR THE PURPOSE FOR WHICH IT WAS
// FURNISHED BY NEC NEC CORPORATION, NO PART OF THIS PROGRAM
// MAY BE REPRODUCED OR DISCLOSED TO OTHERS, IN ANY FORM
// WITHOUT THE PRIOR WRITTEN PERMISSION OF NEC NEC CORPORATION.
//
// NEC CORPORATION CONFIDENTIAL AND PROPRIETARY
package com.nec.jp.systemdirector.ent.msa.customer.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.cors.CorsConfiguration;
import com.nec.jp.systemdirector.ent.msa.customer.resources.dto.CustomerGetDto;
import com.nec.jp.systemdirector.ent.msa.customer.resources.dto.CustomerGetResponseDto;
import com.nec.jp.systemdirector.ent.msa.customer.resources.dto.CustomerPostDto;
import com.nec.jp.systemdirector.ent.msa.customer.resources.dto.CustomerPostResponseDto;
import com.nec.jp.systemdirector.ent.msa.customer.resources.http.SecureHttpHeaders;
import com.nec.jp.systemdirector.ent.msa.customer.services.CustKanaService;
import com.nec.jp.systemdirector.ent.msa.customer.services.dto.CustomerDto;

/**
 * API v1 処理を実装するクラス。<br />
 * URI が <code>/api/v1/localized/customer</code> の API 処理を実装する。
 *
 */
@RestController
@Validated
@RefreshScope
@RequestMapping("${api.base-path}/customers")
@CrossOrigin(origins = { "http://localhost:9000" }, methods = { RequestMethod.POST, RequestMethod.GET,
	RequestMethod.PUT,
	RequestMethod.DELETE }, allowCredentials = "true", allowedHeaders = CorsConfiguration.ALL, exposedHeaders = {}, maxAge = 1800)
public class CustomerController {

    /** ロガー */
    @SuppressWarnings("squid:S1068")
    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustKanaService custKanaService;

    // 顧客カナ照会処理
    @RequestMapping(value = "/{cif}/kana", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, headers = "X-Requested-With=XMLHttpRequest")
    @NewSpan("CustomerController.get")
    public ResponseEntity<CustomerGetResponseDto> get(@PathVariable("cif") @SpanTag("cif") String cif,
	    @RequestBody(required = false) @SpanTag("RequestBody") CustomerGetDto dto) {
	SecureHttpHeaders headers = new SecureHttpHeaders();

	CustomerDto serviceDto = new CustomerDto();
	serviceDto.setBranchNumber(cif.substring(0, 4));
	serviceDto.setCustomerNumber(cif.substring(4));
	if (dto != null) {
	    BeanUtils.copyProperties(dto, serviceDto);
	}

	// CustKanaServiceの呼び出し
	CustomerDto retServiceDto = custKanaService.inquiry(serviceDto);
	if (retServiceDto == null) {
	    return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
	}

	CustomerGetResponseDto retDto = new CustomerGetResponseDto();
	BeanUtils.copyProperties(retServiceDto, retDto);
	headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
	return new ResponseEntity<>(retDto, headers, HttpStatus.OK);
    }

    // 顧客登録処理
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "X-Requested-With=XMLHttpRequest")
    @NewSpan("CustomerController.post")
    public ResponseEntity<CustomerPostResponseDto> post(
	    @SpanTag("RequestBody") @RequestBody @Validated CustomerPostDto dto) {
	SecureHttpHeaders headers = new SecureHttpHeaders();

	headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
	CustomerDto serviceDto = new CustomerDto();
	BeanUtils.copyProperties(dto, serviceDto);

	// CustKanaServiceの呼び出し
	CustomerDto retServiceDto = custKanaService.register(serviceDto);

	if (retServiceDto == null) {
	    return new ResponseEntity<>(null, headers, HttpStatus.CONFLICT);
	}

	CustomerPostResponseDto retDto = new CustomerPostResponseDto();
	BeanUtils.copyProperties(retServiceDto, retDto);

	// 結果の返却
	return new ResponseEntity<>(retDto, headers, HttpStatus.CREATED);
    }


}
